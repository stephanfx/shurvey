<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Shurvey
 * @subpackage Shurvey/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Shurvey
 * @subpackage Shurvey/public
 * @author     Your Name <email@example.com>
 */
class Shurvey_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $shurvey    The ID of this plugin.
	 */
	private $shurvey;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $shurvey       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $shurvey, $version ) {

		$this->shurvey = $shurvey;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Shurvey_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Shurvey_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->shurvey, plugin_dir_url( __FILE__ ) . 'css/shurvey-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Shurvey_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Shurvey_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->shurvey, plugin_dir_url( __FILE__ ) . 'js/shurvey-public.js', array( 'jquery' ), $this->version, false );

	}

	public function shurvey_api_init()
	{
		/**
		 * Add the api interface to the public hooks section
		 */
		global $shurvey_api;

		$shurvey_api = new Shurvey_API();

		add_filter( 'json_endpoints', array($shurvey_api, 'register_routes'));

	}

}
