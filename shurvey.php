<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Shurvey
 *
 * @wordpress-plugin
 * Plugin Name:       Shurvey - Survey tool
 * Plugin URI:        http://example.com/shurvey-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Stephan Grobler
 * Author URI:        http://example.com/
 * License:           Commercial
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       shurvey
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-shurvey-activator.php
 */
function activate_shurvey() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-shurvey-activator.php';
	Shurvey_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-shurvey-deactivator.php
 */
function deactivate_shurvey() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-shurvey-deactivator.php';
	Shurvey_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_shurvey' );
register_deactivation_hook( __FILE__, 'deactivate_shurvey' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-shurvey.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_shurvey() {

	$plugin = new Shurvey();
	$plugin->run();

}
run_shurvey();

// This function is to help with the redirect errors that happen in this plugin.
// Shamelessly copied from : https://tommcfarlin.com/wp_redirect-headers-already-sent/
function app_output_buffer() {
	ob_start();
} // soi_output_buffer
add_action('init', 'app_output_buffer');