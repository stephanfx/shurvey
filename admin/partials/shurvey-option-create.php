<div class="wrap">
    <h1>
        <?php echo $heading; ?>
        <a href="admin.php?page=question-create&id=<?php echo esc_html($data['question_id']); ?>" class="page-title-action">Back to question</a>
    </h1>
    <hr>
    <form method="POST" action="admin.php?page=option-create">
        <label for="question">Option</label>
        <input type="text" name="option_desc" value="<?php echo $data['option_desc']; ?>">
        <input type="submit" name="submit" value="Submit" class="button-primary">
        <input type="hidden" value="<?php echo $data['id']; ?>" name="id">
        <input type="hidden" value="<?php echo $data['question_id']; ?>" name="question_id">
    </form>
</div>
