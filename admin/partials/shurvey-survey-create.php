<?php
/**
 * Created by PhpStorm.
 * User: Stephan
 * Date: 2/2/2016
 * Time: 10:40 PM
 */
?>
<div class="wrap">
<style>
    .form-group {
        display: block;
        width: 100%;
        margin-bottom: 20px;
    }

    .form-group label {
        display: block;
        width:100%;
        margin-bottom: 10px;
    }

    .form-control{
        padding: 5px 10px;
        width: 100%;
    }
</style>
    <div>
        <h1>
        <?php echo $heading; ?>
         <a href="admin.php?page=shurvey" class="page-title-action">Back to survey list </a>
         </h1>
    </div>
    <form method="POST" action="admin.php?page=survey-create">
        <div class="form-group">
            <label for="survey_name">Survey Name</label>
            <input class="form-control" name="name" value="<?php echo $data['name']; ?>" type="text">
        </div>
        <div class="form-group">
            <input type="submit" value="Submit" class="button-primary"/>
        </div>
        <?php if ($data['id']) : ?>
            Current Code: <?php echo $data['code']; ?>
        <?php endif; ?>
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
        <input type="hidden" name="page" value="survey-create">
    </form>
    <?php if ($data['id']) { ?>
    <hr>
    <h1>Questions
        <span>
            <a href="admin.php?page=question-create&survey_id=<?php echo esc_html($data['id']); ?>" class="button-primary">Add New Question</a>
        </span>
    </h1>
    <table class="widefat">
        <thead>
            <tr>
                <th>id</th>
                <th>Question</th>
                <th>Responses</th>
                <th>Active</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $question): ?>
                <tr>
                    <td><?php echo esc_html($question['id']); ?></td>
                    <td>
                        <a href="admin.php?page=question-create&id=<?php echo esc_html($question['id']); ?>&survey_id=<?php echo esc_html($data['id']); ?>">
                            <?php echo esc_html($question['question']); ?>
                        </a>
                    </td>
                    <td>
                        <?php echo esc_html($question['responses']); ?>
                    </td>
                    <td>
                        <?php if ($question['active']) : ?>
                            <span class="dashicons dashicons-yes"></span>
                        <?php else: ?>
                            <span class="dashicons dashicons-no"></span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($question['active']): ?>
                            <a href="admin.php?page=question-create&id=<?php echo esc_html($question['id']); ?>&survey_id=<?php echo esc_html($data['id']); ?>&action=close_question">
                            Close Question
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php } // end if ($data['id']) ?>
</div>
