<div class="wrap">
<style>
    .form-group {
        display: block;
        width: 100%;
        margin-bottom: 20px;
    }

    .form-group label {
        display: block;
        width:100%;
    }

    .form-control{
        padding: 5px 10px;
        width: 100%;
    }
</style>
    <h1>
        <?php echo $heading; ?>
        <a href="admin.php?page=survey-create&id=<?php echo esc_html($data['survey_id']); ?>" class="page-title-action">Back to survey
        </a>
    </h1>
    <hr>
    <form method="POST" action="admin.php?page=question-create">
        <div class="form-group">
            <label for="question">Question</label>
            <input type="text" class="form-control" name="question" value="<?php echo $data['question']; ?>">
        </div>

    <?php if (count($data['options']) == 0) : ?>
        <div class="form-group">
            <label for="options">Options</label>
            <textarea class="form-control" name="options" rows="10">Options (1 per line)</textarea>
        </div>
    <?php endif; ?>
        <input type="submit" name="submit" value="Submit" class="button-primary">
        <input type="hidden" value="<?php echo $data['id']; ?>" name="id">
        <input type="hidden" value="<?php echo $data['survey_id']; ?>" name="survey_id">
    </form>

    <hr>
    <?php if (count($data['options']) > 0) : ?>
    <h2>
        Options
         <a href="admin.php?page=survey-question-graph&id=<?php echo esc_html($data['id']); ?>" class="page-title-action">View Graph
        </a>
    </h2>
    <table class="widefat">
        <thead>
        <tr>
            <td>ID</td>
            <td>Option</td>
            <td>Date Created</td>
            <td>Responses</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data['options'] as $option): ?>
        <tr>
            <td><?php echo esc_html($option['id']); ?></td>
            <td>
                <a href="admin.php?page=option-create&id=<?php echo esc_html($option['id']); ?>&question_id=<?php echo esc_html($data['id']); ?>">
                    <?php echo esc_html($option['option_desc']); ?>
                </a>
            </td>
            <td>
                <?php echo esc_html($option['created_at']); ?>
            </td>
            <td>
                <?php echo esc_html($option['responses']); ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
</div>
