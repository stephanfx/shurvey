<?php
/**
 * Created by PhpStorm.
 * User: Stephan
 * Date: 2/2/2016
 * Time: 10:37 PM
 */


?>

<div class="wrap">
    <h1>Survey List</h1>
    <table class="widefat">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>CODE</th>
            <th>Active</th>
            <th>Options</th>
            <th>Date Created</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($results as $row): ?>
        <tr>
            <td><?php echo esc_html($row['id']); ?></td>
            <td>
                <a href="admin.php?page=survey-create&id=<?php echo esc_html($row['id']); ?>">
                    <?php echo esc_html($row['name']); ?>
                </a>
            </td>
            <td><?php echo esc_html($row['code']); ?></td>
            <td>
                <?php if ($row['active']) : ?>
                    <span class="dashicons dashicons-yes"></span>
                <?php else: ?>
                    <span class="dashicons dashicons-no"></span>
                <?php endif; ?>
            </td>
            <td>
                <?php if ($row['active']) : ?>
                    <a href="admin.php?page=survey-create&id=<?php echo esc_html($row['id']); ?>&action=close_survey">
                        Close Survey/Meeting
                    </a>
                <?php endif; ?>
                <?php if (!$row['deleted_at']) : ?>
                    <a href="admin.php?page=survey-create&id=<?php echo esc_html($row['id']); ?>&action=delete_survey">
                        Delete Survey/Meeting
                    </a>
                <?php endif; ?>
            </td>
            <td><?php echo esc_html($row['created_at']); ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
