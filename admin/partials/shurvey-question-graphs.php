<div class="wrap">
<style>
    #placeholder {
        background: #888;
    }
</style>
    <h1>
        <?php echo $heading; ?>
        <a href="admin.php?page=question-create&id=<?php echo intval($question_id) ; ?>" class="page-title-action">Back to survey
        </a>
    </h1>
    <hr>
    <div id="placeholder" style="height: 400px;"></div>

    <script>
    (function($){
        $(document).ready(function(){
            data = <?php echo json_encode($data); ?>;
            function labelFormatter(label, series) {
                return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
            }
            $.plot('#placeholder', data, {
                series: {
                    pie: {
                        show: true,
                        label: {
                            show: true,
                            formatter: labelFormatter,
                            background: {
                                opacity: 0.5,
                                color: "#000"
                            }
                        }
                    }
                }
            });
        });
    })(jQuery);

    </script>

</div>
