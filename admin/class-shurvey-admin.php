<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Shurvey
 * @subpackage Shurvey/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Shurvey
 * @subpackage Shurvey/admin
 * @author     Your Name <email@example.com>
 */
class Shurvey_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $shurvey    The ID of this plugin.
	 */
	private $shurvey;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $shurvey       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $shurvey, $version ) {

		$this->shurvey = $shurvey;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Shurvey_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Shurvey_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->shurvey, plugin_dir_url( __FILE__ ) . 'css/shurvey-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Shurvey_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Shurvey_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->shurvey, plugin_dir_url( __FILE__ ) . 'js/shurvey-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->shurvey . "flot", plugin_dir_url( __FILE__ ) . 'js/flot/jquery.flot.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( $this->shurvey . "flot.pie", plugin_dir_url( __FILE__ ) . 'js/flot/jquery.flot.pie.js', array( 'jquery' ), $this->version, false );

	}

	public function shurvey_menu()
	{
		add_menu_page('Shurvey Page', 'Shurveys', 'manage_options', 'shurvey', array($this, 'shurvey_survey_list'));
		add_submenu_page('shurvey', 'Add Surveys', 'Add Surveys', 'manage_options', 'survey-create', array($this, 'shurvey_survey_create'));
		add_submenu_page('shurvey', null, null, 'manage_options', 'question-create', array($this, 'shurvey_question_create'));
		add_submenu_page('shurvey', null, null, 'manage_options', 'option-create', array($this, 'shurvey_option_create'));
		add_submenu_page('shurvey', null, null, 'manage_options', 'survey-question-graph', array($this, 'shurvey_question_graph'));
	}

	public function shurvey_survey_list()
	{
		// get all the created surveys and list them
		global $wpdb;
		$table_name = $wpdb->prefix . 'shurvey_surveys';

		$results = $wpdb->get_results("SELECT * FROM $table_name where deleted_at IS NULL ORDER BY created_at DESC", ARRAY_A);

		require plugin_dir_path(__FILE__) . '/partials/shurvey-survey-list.php';
	}

	public function shurvey_survey_create()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'shurvey_surveys';

		if (isset($_POST['name'])){
			$random_code = strtoupper(substr(uniqid(), 8));

			// data to be inserted into survey table
			$heading = "Edit Survey";

			$data = array(
				'id' => intval( $_POST['id']),
				'name' => sanitize_text_field( $_POST['name']),
				'code' => $random_code
				);
			if ($data['id'] == 0){
				$wpdb->insert($table_name, $data);
			} else {
				$wpdb->update($table_name, $data, array('id'=>$data['id']));
			}
			wp_redirect(admin_url() . 'admin.php?page=survey-create&id=' . $wpdb->insert_id);
		} else if (isset($_GET['id']) && isset($_GET['action'])) {
			// set record as inactive
			if ($_GET['action'] == "close_survey") {
				$data = array(
					'id' => intval($_GET['id']),
					'active' => 0
				);
				$wpdb->update($table_name, $data, array('id'=>$data['id']));
				wp_redirect(admin_url() . 'admin.php?page=shurvey');
			}
			if ($_GET['action'] == "delete_survey") {
				$data = array(
					'id' => intval($_GET['id']),
					'active' => 0,
					'deleted_at' => date('Y-m-d H:i:s')
				);
				$wpdb->update($table_name, $data, array('id'=>$data['id']));
				wp_redirect(admin_url() . 'admin.php?page=shurvey');
			}
		} else if (isset($_GET['id'])) {
			//retrieve edit record
			$heading = 'Edit Survey';
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE id = " . $_GET['id'], ARRAY_A);
			if (isset($results[0])){
				$data = $results[0];
			} else {
				$err = "Record not found";
			}
		} else {
			$heading = 'Add New Survey';
			$data = array(
				'id' => 0,
				'name' => ''
				);
		}

		// question list
		$question_table = $wpdb->prefix . 'shurvey_questions';
		$responses_table = $wpdb->prefix . 'shurvey_responses';
		// get all the questions linked to this survey and all responses for questions, should there be any
		$query = "SELECT q.id, q.question, q.active, count(r.id) as responses FROM $question_table q ";
		$query .= " LEFT JOIN $responses_table r ON q.id = r.question_id  WHERE q.survey_id = " . $data['id'];
		$query .= " GROUP BY q.id ORDER BY q.created_at DESC";
		$results = $wpdb->get_results($query, ARRAY_A);

		require plugin_dir_path(__FILE__) . '/partials/shurvey-survey-create.php';
	}

	// This is the /question-create screen and endpoint
	public function shurvey_question_create()
	{
		global $wpdb;
		$heading = "Question";
		$table_name = $wpdb->prefix . 'shurvey_questions';
		$option_table = $wpdb->prefix . 'shurvey_options';
		// Check to see if this is a post to the form
		if (isset($_POST['question'])){
			// set all questions for this survey inactive
			$data = array(
				'active' => 0
			);
			$wpdb->update($table_name, $data, array('survey_id' => intval($_POST['survey_id'])));

			// insert new question
			$data = array(
				'id' => intval($_POST['id']),
				'survey_id' => intval($_POST['survey_id']),
				'question' => sanitize_text_field($_POST['question']),
				);
			if ($data['id'] == 0){
				$wpdb->insert($table_name, $data);
				$data['id'] = $wpdb->insert_id;
			} else {
				$wpdb->update($table_name, $data, array('id'=>$data['id']));
			}
			// insert options
			$options = explode("\r\n", $_POST['options']);
			$this->add_options($data['id'], $options);

			$data['options'] = $this->get_question_options($data['id']);
		} else if (isset($_GET['id']) && isset($_GET['action'])) {
			// handle all question related actions
			$action = sanitize_text_field($_GET['action']);
			// close the question so it does not receive any more response of late joiners.
			if ("close_question" == $action){
				$data = array(
					'id' => intval($_GET['id']),
					'active' => 0
				);
				$wpdb->update($table_name, $data, array('id' => $data['id']));
				$survey_id = intval($_GET['survey_id']);
				wp_redirect(admin_url() . 'admin.php?page=survey-create&id=' . $survey_id);
			}

		} else if (isset($_GET['id'])){
			// get the related data
			$heading = 'Edit Question';
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE id = " . $_GET['id'], ARRAY_A);
			if (isset($results[0])){
				$data = $results[0];
				// select options

				$data['options'] = $this->get_question_options($data['id']);
			} else {
				$err = "Record not found";
			}

		} else {
			$data = array(
				'id' => 0,
				'survey_id' => isset($_GET['survey_id']) ? intval($_GET['survey_id']) : null,
				'question' => 'New question',
				'options' => []
				);
		}
		require plugin_dir_path(__FILE__) . '/partials/shurvey-question-create.php';
	}

	private function get_question_options($question_id){
		global $wpdb;
		$option_table = $wpdb->prefix . 'shurvey_options';
		$response_table = $wpdb->prefix . 'shurvey_responses';
		// Get all the options for a question and a response count for any and all responses
		$query = "SELECT o.id, o.option_desc, o.created_at, count(r.id) as responses FROM $option_table o";
		$query .= " LEFT JOIN $response_table r ON r.option_id = o.id";
		$query .= " WHERE o.question_id = " . intval($question_id);
		$query .= " GROUP BY o.id";
		$results = $wpdb->get_results($query, ARRAY_A);
		return $results;
	}

	private function add_options($question_id, $options){
		// check if they exist
		global $wpdb;
		$table_name = $wpdb->prefix . 'shurvey_options';
		foreach ($options as $option){
			// select from the db
			$query = "SELECT * FROM $table_name WHERE option_desc = '" . $option . "' AND question_id = $question_id";
			$db_option = $wpdb->get_row($query);
			if (!$db_option){
				$data = array(
					'question_id' => $question_id,
					'option_desc' => $option
					);
				$wpdb->insert($table_name, $data);
			}
		}
	}


	public function shurvey_option_create()
	{
		global $wpdb;
		$heading = "Options";
		$table_name = $wpdb->prefix . 'shurvey_options';
		if (isset($_POST['option_desc'])){
			$data = array(
				'id' => intval($_POST['id']),
				'question_id' => intval($_POST['question_id']),
				'option_desc' => sanitize_text_field($_POST['option_desc']),
				);
			if ($data['id'] == 0){
				$stuff = $wpdb->insert($table_name, $data);
				if (!$stuff){

				}
				echo "Successfull entry";
			} else {
				$wpdb->update($table_name, $data, array('id'=>$data['id']));
			}

		} else if (isset($_GET['id'])){
			// get the related data
			$heading = 'Edit Option';
			$results = $wpdb->get_results("SELECT * FROM $table_name WHERE id = " . $_GET['id'], ARRAY_A);
			if (isset($results[0])){
				$data = $results[0];
			} else {
				$err = "Record not found";
			}

		} else {
			$heading = "Add Option";
			$data = array(
				'id' => 0,
				'question_id' => isset($_GET['question_id']) ? intval($_GET['question_id']) : null,
				'option_desc' => 'New option',
				);

		}
		require plugin_dir_path(__FILE__) . '/partials/shurvey-option-create.php';

	}

	public function shurvey_question_graph()
	{
		global $wpdb;
		$question_id = intval($_GET['id']);
		$heading = "";
		$option_table = $wpdb->prefix . 'shurvey_options';
		$response_table = $wpdb->prefix . 'shurvey_responses';
		// Get all the options for a question and a response count for any and all responses
		$query = "SELECT o.option_desc as label, count(r.id) as data FROM $option_table o";
		$query .= " LEFT JOIN $response_table r ON r.option_id = o.id";
		$query .= " WHERE o.question_id = " . intval($question_id);
		$query .= " GROUP BY o.id";
		$data = $wpdb->get_results($query, ARRAY_A);



		require plugin_dir_path(__FILE__) . '/partials/shurvey-question-graphs.php';
	}
}
