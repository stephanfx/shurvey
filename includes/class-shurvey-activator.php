<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Shurvey
 * @subpackage Shurvey/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Shurvey
 * @subpackage Shurvey/includes
 * @author     Your Name <email@example.com>
 */
class Shurvey_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		self::create_survey_table();
		self::create_question_table();
		self::create_option_table();
		self::create_survey_responses_table();
	}

	public function create_survey_table()
	{
		global $wpdb;

		$table_name = $wpdb->prefix . 'shurvey_surveys';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  name varchar(55) NOT NULL,
		  active tinyint(1) DEFAULT 1,
		  code varchar(55) NULL,
		  created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
		  deleted_at datetime,
		  PRIMARY KEY  id  (id)
) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta( $sql );

	}

	public function create_question_table(){
		global $wpdb;

		$table_name = $wpdb->prefix . 'shurvey_questions';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  survey_id int(11) NOT NULL,
		  question varchar(55) NOT NULL,
		  active tinyint(1) DEFAULT 1,
		  created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
		  deleted_at datetime,
		  PRIMARY KEY  id  (id)
) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta( $sql );

	}

	public function create_option_table()
	{

		global $wpdb;

		$table_name = $wpdb->prefix . 'shurvey_options';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  question_id int(11) NOT NULL,
		  option_desc varchar(55) NOT NULL,
		  active tinyint(1) DEFAULT 1,
		  created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
		  deleted_at datetime,
		  PRIMARY KEY  id  (id)
) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta( $sql );
	}

	public function create_survey_responses_table()
	{

		global $wpdb;

		$table_name = $wpdb->prefix . 'shurvey_responses';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  survey_id int(11) NOT NULL,
		  question_id int(11) NOT NULL,
		  option_id int(11) NOT NULL,
		  user_id int(11) NOT NULL,
		  active tinyint(1) DEFAULT 1,
		  created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
		  deleted_at datetime,
		  PRIMARY KEY  id  (id)
) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta( $sql );
	}

}
