<?php

class Shurvey_API {
    public function register_routes ( $routes ){
        $routes['/shurveys'] = array(
            array( array( $this, 'get_posts' ), WP_JSON_Server::READABLE),
        );

        $routes['/shurveys/(?P<id>\d+)'] = array(
            array( array($this, 'get_post'), WP_JSON_Server::READABLE),
            array( array($this, 'answer_survey'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON)
        );

        $routes['/shurveys-result/(?P<id>\d+)'] = array(
            array( array($this, 'get_results'), WP_JSON_Server::READABLE),
        );

        $routes['/shurveys/join'] = array(
            array( array( $this, 'join_survey' ), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON),
        );

        return $routes;

    }

    public function get_posts()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'shurvey_surveys';

        $results = $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);


        return $results;
    }

    public function join_survey( $data )
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'shurvey_surveys';
        $query = "SELECT * FROM $table_name";

        if ($data['code']){
           $code = sanitize_text_field($data['code']);
           $query .= " WHERE $table_name.code = '$code' AND active = 1";
        }

        $results = $wpdb->get_results($query, ARRAY_A);

        return $results;
    }

    public function get_post( $id )
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'shurvey_surveys';

        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE id = $id", ARRAY_A);

        // Get all related questions
        if ($result){
            $question_table = $wpdb->prefix . 'shurvey_questions';

            $survey_id = $result[0]['id'];
            $questions = $wpdb->get_results("SELECT * FROM $question_table WHERE survey_id = $survey_id AND active = 1", ARRAY_A);

            // get all options for the questions
            if ($questions){
                $option_table = $wpdb->prefix . 'shurvey_options';
                foreach ($questions as $index => $question){
                    $qid = $question['id'];
                    $option_result = $wpdb->get_results("SELECT * FROM $option_table WHERE question_id = $qid", ARRAY_A);
                    $questions[$index]['options'] = $option_result;
                }
            }

            $result[0]['questions'] = $questions;
        }

        return $result[0];
    }

    public function answer_survey( $data, $id )
    {

        // test entry
        global $wpdb;

        $data['survey_id'] = $id;
        $result_table = $wpdb->prefix . 'shurvey_responses';

        // get the correct data from the data passed in
        $survey_id = intval($id);
        $cleanData = array(
            'survey_id' => $survey_id,
            'question_id' => intval($data['id']),
            'option_id' => intval($data['answer']),
        );
        $success = $wpdb->insert($result_table, $cleanData);
        return "success";

    }

    public function get_results($id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'shurvey_surveys';

        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE id = $id", ARRAY_A);

        // Get all related questions
        if ($result){
            $question_table = $wpdb->prefix . 'shurvey_questions';

            $survey_id = $result[0]['id'];
            $questions = $wpdb->get_results("SELECT * FROM $question_table WHERE survey_id = $survey_id ORDER BY created_at DESC", ARRAY_A);

            // get all options for the questions
            if ($questions){
                $option_table = $wpdb->prefix . 'shurvey_options';
                foreach ($questions as $index => $question){
                    $qid = $question['id'];
                    $option_result = $wpdb->get_results("SELECT * FROM $option_table WHERE question_id = $qid", ARRAY_A);
                    $questions[$index]['options'] = $option_result;

                    // get any and all results
                    $result_table = $wpdb->prefix . 'shurvey_responses';

                    $query = "Select count(1) as total_option_count, $result_table.* from $result_table".
                        " where survey_id = $survey_id AND question_id = $qid GROUP BY option_id";

                    $results = $wpdb->get_results($query, ARRAY_A);
                    $questions[$index]['results'] = $results;

                }

            }

            $result[0]['questions'] = $questions;
        }

        return $result[0];
        return $results;
    }
}